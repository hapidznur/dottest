# DotTest

1. Apa tantangan terbesar yang pernah Anda temui saat membuat web application dari sisi engineering dan bagaimana menyelesaikan permasalahan tersebut?.
2. Apakah Anda sudah mengetahui tentang clean code? Bagaimana implementasi clean code pada project Anda?.
3. Apakah Anda menggunakan Git workflow dalam pengerjaan project? Jika ya, jelaskan bagaimana Git workflow yang Anda terapkan.
4. Apa yang anda ketahui dengan design pattern? Jika pernah menggunakan, jelaskan design pattern apa saja yang biasanya digunakan untuk menyelesaikan masalah software engineering di web application.
5. Apa anda bersedia ditempatkan onsite di Malang? Jika memang harus remote, bagaimana pengaturan waktu & availability dalam komunikasi dan kolaborasi pengerjaan project?.

1. Saat tidak menemukan solusi untuk sisi technical atau logic. Cara menyelesaikannya dengan bertanya dan berdiskusi dengan team. 
2. Clean code adalah dokumentasi teknis berupa code yang bisa dibaca oleh developer lain. Implementasinya adalah penamaan variable, fungsi yang sesuai dengan tujuan dibuat variable atau fungsi tersebut.
3. Pernah. Sebelumnya saya pernah memakai git workflow pada project. Terakhir saya ingin menerapkan Git Trunk Base development pada team namun gagal karena team belum sepenuhnya paham tentang git. 
4. design pattern digunakan untuk manajamen code dari sebuah projek. Penggunaan design pattern akan memudahkan team mengerjakan projek. Design pattern yang pernah saya gunakan mvc dan mvp pada Django. Pada laravel hanya pernah belajar BaseRepository namun hanya 1 bulan ini. 
5. Bersedia.
